package com.tritonsoft.photho_form;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.lang.reflect.Method;

public class MainActivity extends AppCompatActivity {

    private static final int TAKE_PICTURE_1 = 1;
    private static final int TAKE_PICTURE_2 = 2;
    private static final int TAKE_PICTURE_3 = 3;
    private static final int TAKE_PICTURE_4 = 4;
    public ImageView btn_take1;
    public ImageView btn_take2;
    public ImageView btn_take3;
    public ImageView btn_take4;

    public Uri imageUri_1;
    public Uri imageUri_2;
    public Uri imageUri_3;
    public Uri imageUri_4;


    public ImageView img_foto1;
    public ImageView img_foto2;
    public ImageView img_foto3;
    public ImageView img_foto4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        //instanciar botones (ImageView)
        btn_take1 = findViewById(R.id.btn_take1);
        btn_take2 = findViewById(R.id.btn_take2);
        btn_take3 = findViewById(R.id.btn_take3);
        btn_take4 = findViewById(R.id.btn_take4);

        //instanciar imageView para mostrar las fotos
        img_foto1 = findViewById(R.id.img_foto1);
        img_foto2 = findViewById(R.id.img_foto2);
        img_foto3 = findViewById(R.id.img_foto3);
        img_foto4 = findViewById(R.id.img_foto4);

        //agregar setOnClickListener para el evento de click de cada boton
        btn_take1.setOnClickListener(v ->{
            //agregar el intent de la camara aqui
            if(hasPermissions())
            {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                File photoFile = new File(Environment.getExternalStorageDirectory(),  "Photo_1.png");
                intent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(photoFile));
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                imageUri_1 = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".GenericFileProvider", photoFile);
                startActivityForResult(intent, TAKE_PICTURE_1);
            }




        });

        btn_take2.setOnClickListener(v ->{
            //agregar el intent de la camara aqui
            if(hasPermissions())
            {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                File photoFile = new File(Environment.getExternalStorageDirectory(),  "Photo_2.png");
                intent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(photoFile));
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                imageUri_2 = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".GenericFileProvider", photoFile);
                startActivityForResult(intent, TAKE_PICTURE_2);
            }

        });

        btn_take3.setOnClickListener(v ->{
            //agregar el intent de la camara aqui
            if(hasPermissions())
            {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                File photoFile = new File(Environment.getExternalStorageDirectory(),  "Photo_3.png");
                intent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(photoFile));
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                imageUri_3 = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".GenericFileProvider", photoFile);
                startActivityForResult(intent, TAKE_PICTURE_3);
            }

        });

        btn_take4.setOnClickListener(v ->{
            //agregar el intent de la camara aqui
            if(hasPermissions())
            {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                File photoFile = new File(Environment.getExternalStorageDirectory(),  "Photo_4.png");
                intent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(photoFile));
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                imageUri_4 = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".GenericFileProvider", photoFile);
                startActivityForResult(intent, TAKE_PICTURE_4);
            }

        });



    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE_1:
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(MainActivity.this,"Se agrego la foto 1",Toast.LENGTH_SHORT).show();
                    setImageViewFrame(imageUri_1,img_foto1);

                }
                break;
            case TAKE_PICTURE_2:
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(MainActivity.this,"Se agrego la foto 2",Toast.LENGTH_SHORT).show();
                    setImageViewFrame(imageUri_2,img_foto2);
                }
                break;
            case TAKE_PICTURE_3:
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(MainActivity.this,"Se agrego la foto 3",Toast.LENGTH_SHORT).show();
                    setImageViewFrame(imageUri_3,img_foto3);
                }
                break;
            case TAKE_PICTURE_4:
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(MainActivity.this,"Se agrego la foto 4",Toast.LENGTH_SHORT).show();
                    setImageViewFrame(imageUri_4,img_foto4);
                }
                break;

        }
    }


    public void setImageViewFrame(Uri uri, ImageView imageView)
    {
        Log.d("setImageViewFrame", uri.getPath());
        Glide.with(this)
                .load(uri)
                .thumbnail(0.1f)
                .into(imageView);
    }

    public boolean hasPermissions()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 9000);
            return false;

        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 9000);
            return false;

        }



        return true;


    }




}
